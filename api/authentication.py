from rest_framework.authentication import BaseAuthentication, get_authorization_header
from rest_framework import exceptions
from django.utils.translation import ugettext_lazy as _
from .models import Profile

class UidAuthentication(BaseAuthentication):
    
    keyword = 'UID'

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if len(auth) == 1:
            msg = _('Invalid uid header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _('Invalid uid header. uid string should not contain spaces.')
            raise exceptions.AuthenticationFailed(msg)

        try:
            uid = auth[1].decode()
        except UnicodeError:
            msg = _('Invalid uid header. uid string should not contain invalid characters.')
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(uid)

    def authenticate_credentials(self, key):
        try:
            uid = Profile.objects.get(uid=key)
        except Profile.DoesNotExist:
            raise exceptions.AuthenticationFailed(_('Invalid uid.'))

        if not uid.user.is_active:
            raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))

        return (uid.user, uid)

