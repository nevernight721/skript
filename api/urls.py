from . import views

from django.urls import path
from rest_framework.authtoken import views as rest_framework_views


urlpatterns = [
    path('get_auth_token/', rest_framework_views.obtain_auth_token, name='get_auth_token'),
    path('signup/', views.signup, name='signup'),
    path('create_story/', views.create_story, name='create_story'),
    path('get_chapter/<int:chapter_id>/', views.get_chapter, name='get_chapter'),
    path('get_stories_by_category/<str:category_id>/', views.get_stories_by_category, name='get_stories_by_category'),
    path('get_story/<int:story_id>/', views.get_story, name='get_story'),
    path('get_viewed_stories/', views.get_viewed_stories, name='get_viewed_stories'),
    path('get_reading_now/', views.get_reading_now, name='get_reading_now'),
    path('set_favorite_categories/', views.set_favorite_categories, name='set_favorite_categories'),
    path('get_all_categories/', views.get_all_categories, name='get_all_categories'),
    path('get_favorite_categories/', views.get_favorite_categories, name='get_favorite_categories'),
    path('get_carousel_of_stories/', views.get_carousel_of_stories, name='get_carousel_of_stories'),
    path('get_recommended_stories/', views.get_recommended_stories, name='get_recommended_stories'),
    path('replenish_balance/', views.replenish_balance, name='replenish_balance')
]
