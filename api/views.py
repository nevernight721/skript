from .models import Story, Profile, Message, Category, Chapter
from .authentication import UidAuthentication

from datetime import datetime, timedelta
import base64

from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.contrib.auth.models import User
from django.core import serializers
from django.forms.models import model_to_dict

from rest_framework.decorators import api_view, authentication_classes, permission_classes, parser_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.parsers import JSONParser


def get_story_dict(story):
    chapters = Chapter.objects.filter(story=story).order_by('order')
    story_dict = model_to_dict(story)
    story_dict['user'] = Profile.objects.get(id=story_dict['user']).user.username
    story_dict['category'] = Category.objects.get(id=story_dict['category']).name
    if chapters:
        story_dict['chapters'] = [model_to_dict(c) for c in chapters]
    else:
        story_dict['chapters'] = []
    return story_dict


@api_view(['GET'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
def get_main_history(request):
    story = Story.objects.filter(is_main_story=True)
    if story:
        story_dict = get_story_dict(story[0])
        return JsonResponse(story_dict)
    else:
        return JsonResponse({'error':'Story not found'}, status=404)


def find_best_stories_in_category(category_name, user):
    stories = Story.objects.filter(category__name=category_name).exclude(id__in = [s.id for s in user.viewed_stories.all()])
    result = []
    for story in stories:
        result.append(get_story_dict(story))
    return result


@api_view(['GET'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
def get_recommended_stories(request):
    user = Profile.objects.get(user=request.user)
    favorite_categories = user.favorite_categories.all()
    stories = []
    for category in favorite_categories:
        stories.extend(find_best_stories_in_category(category.name, user))
    return JsonResponse(stories, safe=False)


@api_view(['GET'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
def get_new_stories(request):
    datetime_ = datetime.now() - timedelta(days=3)
    stories = Story.objects.filter(time_publication__gte=datetime_).order('time_publication')
    return HttpResponse(serializers.serialize('json', stories), content_type="application/json")


@api_view(['GET'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
def get_carousel_of_stories(request):
    stories = Story.objects.filter(is_in_carousel=True)
    return HttpResponse(serializers.serialize('json', stories), content_type="application/json")


@api_view(['GET'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
def get_all_categories(request):
    return HttpResponse(serializers.serialize('json', Category.objects.all().order_by('order')), content_type="application/json")


@api_view(['GET'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
def get_favorite_categories(request):
    return HttpResponse(serializers.serialize('json', Profile.objects.get(user__username=request.user.username).favorite_categories.all().order_by('order')), content_type="application/json")


@api_view(['POST'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
@parser_classes((JSONParser,))
def set_favorite_categories(request):
    categories = request.data
    user = Profile.objects.filter(user__username=request.user.username)
    if not user:
        return JsonResponse({'error':'User not found.'}, status=404)
    categories_ = []
    for category in categories:
        categories_.extend(list(Category.objects.filter(name=category)))
    user = user[0]
    user.favorite_categories.set(categories_)
    return JsonResponse({'status':'Success.'})


@api_view(['POST'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
@parser_classes((JSONParser,))
def create_story(request):
    story = request.data
    chapters = story.pop('chapters')
    characters = story.pop('characters')
    preview = story.pop('preview')

    user = Profile.objects.filter(user__username=request.user.username)
    if not user:
        return JsonResponse({'error':'User not found.'}, status=404)
    user = user[0]
    
    preview_url = ''
    if preview:    
        preview_url = save_file(preview, story['name'] + '_preview_image')

    category = Category.objects.filter(name=story['category'].strip())
    if not category:
        category = Category.objects.create(name=story['category'].strip())
        category.save()
    else:
        category = category[0]
    
    story_db = Story.objects.create(name=story['name'].strip(), user=user, \
                                    category=category, preview=preview_url)
    
    story_db.publish()
    previous_chapter = None
    
    for c,i in zip(chapters, range(len(chapters))):    
        background_url = ''
        if c['background']:
            background_url = save_file(c['background'], story['name'] + '_' + c['name'] + '_' + 'background_image')

        chapter_db = Chapter.objects.create(name=c['name'].strip(), story=story_db, previous_chapter=previous_chapter,\
                                            condition=c['condition'].strip(), order=i, background_image=background_url)

        chapter_db.save()
        previous_chapter = chapter_db
        for m, j in zip(c['messages'], range(len(c['messages']))):
            
            attached_url = ''
            if m['attached']:
                attached_url = save_file(m['attached'], story['name'] + '_chapter_' + str(i) + '_message_' + str(j))

            message = Message.objects.create(chapter=chapter_db, character=m['character'].strip(),\
                                            color=characters[m['character'].strip()], order=j, \
                                            attached=attached_url,  text=m['text'].strip(),\
                                            is_ad=m['is_ad'], is_break=m['is_break'], attached_type=m['attached_type'])
            message.save()

    story_db.first_chapter = Chapter.objects.filter(story=story_db).order_by('order')[0]
    story_db.publish()
    return JsonResponse({'response':'Success'})


@api_view(['POST'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
@parser_classes((JSONParser,))
def edit_story(request, story_id):
    story_in = request.data
    story_db = Story.objects.filter(id=story_id)
    if not story_db:
        return JsonResponse({'error':'Story not found.'}, status=404)
    story_db = story_db[0]
    if 'name' in story_in:
        story_db.name = story_in['name']
    if 'preview' in story_in:
        with open(settings.STATIC_ROOT + '/' + story_db.name + '_preview_image', "wb") as fh:
            fh.write(story_in['preview'].decode('base64'))
    
        preview_url = settings.STATIC_URL + '/' + story_db.name + '_preview_image'
        story_db.preview = preview_url
    story_db.save()
    return JsonResponse({'status':'Success.'})


@api_view(['GET'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
def get_stories_by_category(request, category_id):
    if not category_id:
        return JsonResponse({'error': 'Category id is required.'}, status=412)
    stories = [ model_to_dict(s) for s in Story.objects.filter(category__id=category_id)]
    for s in stories:
        s['category'] = Category.objects.get(id=s['category']).name
        s['user'] = User.objects.get(id=s['user']).username
    return JsonResponse(stories, safe=False)


@api_view(['GET'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
def get_story(request, story_id):
    if not story_id:
        return JsonResponse({'error': 'Story id is required.'}, status=412)
    story = Story.objects.filter(id=story_id)
    if not story:
        return JsonResponse({'error':'Story not found.'}, status=404)
    story_dict = get_story_dict(story[0])
    return JsonResponse(story_dict)


@api_view(['GET'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
def get_chapter(request, chapter_id=None):
    if not chapter_id:
        return JSONParser({'error': 'Chapter id is required.'}, status=412)
    chapter = Chapter.objects.filter(id=chapter_id)
    if chapter:
        user = request.user
        profile = Profile.objects.get(user=user) 
        if chapter[0].cost > profile.balance:
            return JsonResponse({'error': 'Not enougth money.'}, status=418)
        profile.balance -= chapter[0].cost
        story = Story.objects.filter(first_chapter__id=chapter_id)
        if story:
            story[0].viewed += 1
            story[0].save()
            user = request.user
            profile = Profile.objects.get(user=user)
            profile.reading_now.add(story[0])
            profile.viewed_stories.add(story[0])
            profile.save()
        chapter_last = story.objects.filter(previous_chapter=chapter)
        if not chapter_last:
            profile.reading_now.remove(chapter.story)
            profile.save()
        chapter_dict = model_to_dict(chapter[0])
        next_chapters = Chapter.objects.filter(previous_chapter__id=chapter_id)
        chapter_dict['next_chapters'] = {c.condition : c.id for c in next_chapters}
        messages = Message.objects.filter(chapter=chapter[0]).order_by('order')
        if messages:
            chapter_dict['messages'] =[model_to_dict(m) for m in messages]
        else:
            chapter_dict['messages'] = []
        
        return JsonResponse(chapter_dict)
    else:
        raise JsonResponse({'error':'Chapter not found.'}, status=404)


@api_view(['GET'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
def get_viewed_stories(request):
    username = request.user.username
    viewed = Profile.objects.get(user__username=username).viewed_stories.all()
    viewed = [model_to_dict(v) for v in viewed]
    for v in viewed:
        v['category'] = Category.objects.get(id=v['category']).name
        v['user'] = User.objects.get(id=v['user']).username
    return JsonResponse(viewed, safe=False)


@api_view(['GET'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
def get_reading_now(request, uid=None):
    username = request.user.username
    reading_now = Profile.objects.get(user__username=username).reading_now.all()
    reading_now = [model_to_dict(v) for v in reading_now]
    for v in reading_now:
        v['category'] = Category.objects.get(id=v['category']).name
        v['user'] = User.objects.get(id=v['user']).username
    return JsonResponse(reading_now, safe=False)


@api_view(['POST'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
@parser_classes((JSONParser,))
def edit_message(request, message_id):
    message_in = request.data
    message_db = Message.objects.filter(id=message_id)
    if not message_db:
        return JsonResponse({'error':'Message not found.'}, status=404)
    message_db = message_db[0]
    if 'character' in message_in:
        message_db.character = message_in['character']
    if 'text' in message_in:
        message_db.text = message_in['text']
    if 'attached' in message_in:
        chapter_db = Chapter.objects.filter(id=message_db.chapter)[0]
        with open(settings.STATIC_ROOT + '/' + story['name'] + '_chapter_' + chapter_db.order + '_message_' + message_db.order, "wb") as fh:
            fh.write(preview.decode('base64'))
        attached_url = settings.STATIC_URL + '/' + story['name'] + '_chapter_' + chapter_db.order + '_message_' + message_db.order
        message_db.attached = attached_url
    if 'is_ad' in message_in:
        message_db.is_ad = message_in['is_ad'] 
    message_db.save()
    return JsonResponse({'status':'Success.'})


@api_view(['POST'])
@authentication_classes((UidAuthentication,))
@permission_classes((IsAuthenticated,))
def replenish_balance(request):
    if 'replenishment' not in request.POST:
        return JsonResponse({'error':'replenishment field is required'}, status=412)
    replenishment = request.POST.get('replenishment')
    user = Profile.objects.get(user=request.user)
    user.balance += replenishment
    user.save()
    return JsonResponse({'status':'Sucssesfull.'})


required_fields = ['first_name', 'last_name', 'username','password','email','uid']

@csrf_exempt
def signup(request):
    if request.method == 'POST':
        if not all([i in request.POST for i in required_fields]):
            return JsonResponse({'error':'Required fields ' + ', '.join(required_fields)})
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        name = request.POST.get('username')
        password = request.POST.get('password')
        email = request.POST.get('email')
        uid = request.POST.get('uid')
        if not User.objects.filter(username = name):
            user = User.objects.create_user(username=name, email=email, password=password, first_name=first_name, last_name=last_name)
            user.save()
            profile = Profile.objects.create(user=user,uid=uid)
            profile.save()
            return JsonResponse({'status':'Success'})
        else:
            return JsonResponse({'error':'User already exists.'}, status=409)
    return JsonResponse({'error':'Method not allowed.'}, status=405)
