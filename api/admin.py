from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Category)
admin.site.register(models.Story)
admin.site.register(models.Chapter)
admin.site.register(models.Message)
admin.site.register(models.Profile)
admin.site.register(models.Subscription)