from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Category(models.Model):
    name = models.CharField(max_length=30)
    order = models.IntegerField(default=0)


class Story(models.Model):
    name = models.CharField(max_length=150)
    preview = models.CharField(max_length=200, null=True, blank=True) # link to static image
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    time_publication = models.DateTimeField(blank=True, null=True)
    user = models.ForeignKey('Profile', null=True,on_delete=models.SET_NULL)
    viewed = models.PositiveIntegerField(default=0)
    first_chapter = models.ForeignKey('Chapter', on_delete=models.SET_NULL, null=True, related_name='start', blank=True)
    is_in_carousel = models.BooleanField(default=False)
    is_main_story = models.BooleanField(default=False)

    def publish(self):
        self.time_publication = timezone.now()
        self.save()


class Chapter(models.Model):
    name = models.CharField(max_length=150)
    story = models.ForeignKey(Story, on_delete=models.CASCADE)
    previous_chapter = models.ForeignKey('Chapter', on_delete=models.SET_NULL, null=True, blank=True)
    condition = models.CharField(max_length=100, default='', blank=True)
    order = models.IntegerField(default=1)
    cost = models.IntegerField(default=0, blank=True)
    background_image = models.CharField(max_length=200, null=True, blank=True)


ATTACHED_TYPES = (
    ('FC', 'Facetime call'),
    ('PH', 'Photo'),
    ('VD', 'Video'),
    ('CL', 'Call'),
)


class Message(models.Model):
    chapter = models.ForeignKey(Chapter, on_delete=models.CASCADE)
    order = models.IntegerField(default=1)
    character = models.CharField(max_length=30)
    color = models.CharField(max_length=6)
    attached = models.CharField(max_length=200, blank=True, default='') # link to static file
    attached_type = models.CharField(max_length=2, choices=ATTACHED_TYPES, blank=True)
    is_ad = models.BooleanField(default=False)
    text = models.CharField(max_length=300)
    is_break = models.BooleanField(default=False)


SUBSCRIPTION_TYPES = ( # TODO tz
    ('WEEK', 'week'),

)


class Subscription(models.Model):
    start_time = models.DateField()
    type = models.CharField(max_length=10,choices=SUBSCRIPTION_TYPES)
    cost = models.PositiveSmallIntegerField()
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE)


class Profile(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    favorite_categories = models.ManyToManyField(Category, blank=True)
    uid = models.CharField(max_length=40)
    viewed_stories = models.ManyToManyField(Story,related_name='stories', blank=True)
    reading_now = models.ManyToManyField(Story, blank=True)
