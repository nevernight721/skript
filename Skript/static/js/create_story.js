Vue.component("color-picker", {
    template: "#color-picker",
    props: ["change", "initial", "h_", "s_", "l_"],
    data: function() {
        return {
        isVisible: false,
        h: this.h_,
        s: this.s_,
        l: this.l_
        }
    },
    computed: {
        color: function() {
            var hsl = hsb2hsl(parseFloat(this.h) / 360, parseFloat(this.s) / 100, parseFloat(this.l) / 100);

            var c = hsl.h + ", " + hsl.s + "%, " + hsl.l + "%";

            var s = "hsl(" + c + ")";
            this.change({
                color: hsl
            });
            return s;
            },
            colorString: function() {
            var c = this.h + ", " + this.s + "%, " + this.l + "%"
            return c;
            },
            gradientH: function() {
            var stops = [];
            for (var i = 0; i < 7; i++) {
                var h = i * 60;
                
                var hsl = hsb2hsl(parseFloat(h / 360), parseFloat(this.s) / 100, parseFloat(this.l / 100))
                
                var c = hsl.h + ", " + hsl.s + "%, " + hsl.l + "%"
                stops.push("hsl(" + c + ")")
            }

            return {
                backgroundImage: "linear-gradient(to right, " + stops.join(', ') + ")"
            }
        },
        gradientS: function() {
            var stops = [];
            var c;
            var hsl = hsb2hsl(parseFloat(this.h / 360), 0, parseFloat(this.l / 100))
            c = hsl.h + ", " + hsl.s + "%, " + hsl.l + "%"
            stops.push("hsl(" + c + ")")

            var hsl = hsb2hsl(parseFloat(this.h / 360), 1, parseFloat(this.l / 100))
            c = hsl.h + ", " + hsl.s + "%, " + hsl.l + "%"
            stops.push("hsl(" + c + ")")

            return {
                backgroundImage: "linear-gradient(to right, " + stops.join(', ') + ")"
            }
        },

        gradientL: function() {
            var stops = [];
            var c;

            var hsl = hsb2hsl(parseFloat(this.h / 360), 0, 0)
            c = hsl.h + ", " + hsl.s + "%, " + hsl.l + "%"
            stops.push("hsl(" + c + ")")

            var hsl = hsb2hsl(parseFloat(this.h / 360), parseFloat(this.s / 100), 1)

            c = hsl.h + ", " + hsl.s + "%, " + hsl.l + "%"
            stops.push("hsl(" + c + ")")

            return {
                backgroundImage: "linear-gradient(to right, " + stops.join(', ') + ")"

            }
        }
    },
    methods: {

        show: function() {
            this.isVisible = true;
        },
        hide: function() {
            this.isVisible = false;
        },
        toggle: function() {
            this.isVisible = !this.isVisible;
        }
    },
});

Vue.component('character', {
    template: "#character-input",
    props: ['index'],
    data: function() {
        var value = "";
        var i = 0;
        
        for(var key in this.$parent.story.characters){
            if(i == this.index-1 && key != "author") {
                value = key;
                i++;
            }else if (key != "author") {
                i++;
            }
        }
        var hsl_ = [Math.random()*360,99,99];
        if(value != ""){
            var rgb = hexToRgb(this.$parent.story.characters[value]);
            hsl_ = rgbToHsl(rgb.r, rgb.g, rgb.b);
        }
        return {
            character: value,
            hsl: {
                h: hsl_[0],
                s: hsl_[1],
                l: hsl_[2],
            }
        }
    },
    methods: {
        delete_character: function(event) {
            event.preventDefault();
            var remove = event.target.parentElement.parentElement;
            remove.parentElement.removeChild(remove);
        },
        select_color: function(event) {
            var hsl_color = event.color;
            var rgb_color = hslToRgb(hsl_color.h/360, hsl_color.s/100, hsl_color.l/100);
            this.color = rgbToHex(rgb_color[0], rgb_color[1], rgb_color[2]);
            this.$parent.colors[this.index-1]= this.color;
        }
    }
});

Vue.component('message', {
    template: "#message",
    props: ['index'],
    data: function() {

        var chapter_index = this.$parent.chapter_index;
        var character_name = "author";
        var text = "";
        var chapters = this.$parent.$parent.story.chapters;
        if(chapters.length > chapter_index-1 && chapters[chapter_index-1].messages.length > this.index-1) {
            var _message = chapters[chapter_index-1].messages[this.index-1]
            character_name = _message.character;
            text = _message.text;
        }
        var color = "background-color:" + this.$parent.$parent.story.characters[character_name];
        return {
            style: color,
            character_name: character_name,
            text: text,
            characters: this.$parent.$parent.story.characters,
            attaching: false,
            base64_attached: "",
            checked: false,
        };
    },
    methods: {
        delete_message: function(event) {
            event.preventDefault();
            
            var remove = event.target.parentElement.parentElement;
            remove.parentElement.parentElement.removeChild(remove.parentElement);
        },
        toggle_attaching: function(event) {
            this.attaching = !this.attaching;
        },
        attach: function() {
            getBase64(this.$refs.attachedFiles.files[0]).then(
                data => {
                    this.base64_attached = data;
                }, error => {
                    console.log(error);
                }
            )
        }
    }
});

Vue.component('chapter', {
    template: "#chapter",
    props: ['index'],
    data: function() {

        var ch_name = "";
        var counter_me = 1;
        if(this.$parent.story.chapters.length > this.index-1) {
            ch_name = this.$parent.story.chapters[this.index-1].name;
            counter_me = this.$parent.story.chapters[this.index-1].messages.length;
        }
        return {
            counter_messages: counter_me,
            chapter_name: ch_name,
            chapter_index: this.index,
            checked: false,
            base64_background_image: "",
        }
    },
    methods: {
        delete_chapter: function(event) {
            var chapter = event.target.parentElement.parentElement.parentElement.parentElement.parentElement;
            chapter.parentElement.removeChild(chapter);
            this.$parent.counter_chapters -= 1;
        },
        uploadBackgroundImage: function() {
            getBase64(this.$refs.backgroundImage.files[0]).then(
                data => {
                    this.base64_background_image = data;
                }, error => {
                    console.log(error);
                }
            )
        }
    }
});


Vue.component('character-button', {
    name: 'character-button',
    template: '#character-button',
    props: ['character_id', 'color'],
    data: function() {
        return {
            characters: this.$parent.characters,
        };
    },
    computed: {
        background_color: function () {
            return "background-color: " + this.color; 
        }
    },
    methods: {
        change_character: function(event) {
            event.preventDefault();
            
            var character_color = this.character_id;
            
            event.target.parentElement.parentElement.style = "background-color:" + character_color;
            this.$parent.character_name = this.get_name_of_character(character_color);
        },
        get_name_of_character: function(color) {
            var name;
            for(var key in this.characters) {
                if(this.characters[key] === color) {
                    name = key;
                    break;
                }
            }
            return name;
        }
    }
});

(function () {
    Vue.config.delimiters = ['[[', ']]'];
    var app = new Vue({
        el: "#app",
        data: {
            story: {
                name: "",
                category:"",
                characters:{
                    author: "#c6f5f0",
                },
                preview:"",
                chapters:[],
            },
            state: "name_story",
            response: "Данные обрабатываются",
            colors: [],
            counter_messages: 1,
            counter_characters: 1,
            counter_chapters: 1,
            name_error: false,
            chapters_name_error: [],
            base64_preview: "",
        },
        methods: {
            attach_preview: function() {
                getBase64(this.$refs.attachedPreview.files[0]).then(
                    data => {
                        this.base64_preview = data;
                    }, error => {
                        console.log(error);
                    }
                )
            },
            name_story_next: function (event) {
                event.preventDefault();
                var name_history = document.getElementById("name-history").value;
                var category_history = document.getElementById("category-history").value;
                var preview_base64 = document.getElementById("base64_preview_text").innerText;
                if(name_history == "") {
                    this.name_error = true;
                    return;
                }
                this.story.name = name_history;
                this.story.category = category_history;
                this.name_error = false;
                this.story.preview = preview_base64;
                this.state = "create_characters";
            },
            save_characters: function() {
                var characters = document.getElementsByClassName("characters")[0].children;
                characters = [].slice.call(characters);
                var ch = {
                    author: "#c6f5f0",
                };
                colors_ = this.colors;
                var counter_ch = 0;
                characters.forEach(function (character, i, arr) {
                    if(character.children[0].children[0].value != '') {
                        counter_ch += 1;
                        ch[character.children[0].children[0].value] = colors_[i];
                        
                    }
                });
                this.story.characters = ch;
                this.counter_characters = counter_ch;
            },
            character_next: function(event) {
                event.preventDefault();
                
                this.save_characters();

                this.state = "create_chapters";
            },
            character_back: function(event) {
                event.preventDefault();

                this.save_characters();

                this.state = "name_story";
            },
            messages_back: function(event) {
                event.preventDefault();

                this.save_chapters();

                this.state = "create_characters";
            },
            save_chapters: function() {
                var chapters = document.getElementById("chapters");
                
                chapters = [].slice.call(chapters.children);
                var chs = [];
                for(var ch in chapters) { 

                    var chapter = {
                        background: chapters[ch].children[0].children[1].children[2].innerText,
                        condition: "",
                    };
                    chapter.name = chapters[ch].children[0].children[0].children[1].value;
                    if(chapter.name == "") {
                        if(!this.chapters_name_error[chapters[ch].id - 1]){            
                            var elem = document.createElement("p");
                            elem.style = "color:red";
                            elem.innerText = "Имя главы не должно быть пустым"
                            chapters[ch].children[0].children[0].appendChild(elem);
                        }
                        this.chapters_name_error[chapters[ch].id - 1] = true;
                        
                        return false;
                    }
                    this.chapters_name_error[chapters[ch].id - 1] = false;
                    if(chs.length == 0) {
                        chapter.previous_chapter = "";
                    }else {
                        chapter.previous_chapter = chs[chs.length-1].name;
                    }

                    chapter.messages = [];
                    var messages = chapters[ch].children[1].children[1].children;
                    messages = [].slice.call(messages);
                    if ( messages[0].tagName != "BUTTON" ){
                        messages.forEach(el => {
                            var attached = "";
                            var attached_type = "";
                            if(el.children[1]) { 
                                attached = el.children[1].children[1].innerText;
                                attached_type = el.children[1].children[2].value;
                            }
                            
                            var text = el.children[0].children[2].children[0].value;

                            var is_break = el.children[0].children[1].innerText == "true";

                            if(text || attached || is_break) {
                                chapter.messages.push({
                                    character: el.children[0].children[0].innerText.trim(),
                                    text: text,
                                    attached: attached,
                                    attached_type: attached_type,
                                    is_ad: false,
                                    is_break: is_break,
                                });
                            }
                        });
                    }
                    chs.push(chapter);
                }
                this.story.chapters = chs;
                return true
            },
            done: function(event) {
                var status = this.save_chapters();
                if(!status){    
                    return;
                }
                
                var vm = this;
                this.$http.post("/admin/create_story", JSON.stringify(this.story)).then(function (response) {
                    vm.response = response.bodyText;
                }, function (response) {
                    vm.response = response.bodyText;
                });
                this.state = 'done';
            }
        }
    });

})();