import json
import base64

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.forms.models import model_to_dict

from api.models import Profile, Category, Story, Chapter, Message


def get_story_dict(story):
    chapters = Chapter.objects.filter(story=story).order_by('order')
    story_dict = model_to_dict(story)
    story_dict['user'] = Profile.objects.get(id=story_dict['user']).user.username
    story_dict['category'] = Category.objects.get(id=story_dict['category']).name
    if chapters:
        story_dict['chapters'] = [model_to_dict(c) for c in chapters]
    else:
        story_dict['chapters'] = []
    return story_dict


def save_file(file_base64, file_name):
    file_type, file_base64 = file_base64.split(',')
    file_type = file_type.split('/')[1].split(';')[0]
    if file_type == 'mpeg':
        file_type = 'mp3'
    with open(settings.STATICFILES_DIRS[0] + '/' + 'uploaded' + '/' + file_name + '.' + file_type, 'wb') as fh:
        fh.write(base64.b64decode(file_base64))
    url = settings.STATIC_URL + 'uploaded' + '/' + file_name + '.' + file_type
    return url

    
@csrf_exempt
def admin_create_story(request):
    if request.user.is_superuser:
        if request.method == 'GET':
            categories = Category.objects.all().values('name')
            categories = list(map(dict, categories))
            return render(request,'create_story.html', context={'categories':categories})
        else:
            request_body = request.body if type(request.body) == type('') else request.body.decode('utf-8')
            story = json.loads(request_body)
            chapters = story.pop('chapters')
            characters = story.pop('characters')
            preview = story.pop('preview')

            user = Profile.objects.filter(user__username=request.user.username)
            if not user:
                return JsonResponse({'error':'User not found.'}, status=404)
            user = user[0]
            
            preview_url = ''
            if preview:    
                preview_url = save_file(preview, story['name'] + '_preview_image')

            category = Category.objects.filter(name=story['category'].strip())
            if not category:
                category = Category.objects.create(name=story['category'].strip())
                category.save()
            else:
                category = category[0]
            
            story_db = Story.objects.create(name=story['name'].strip(), user=user, \
                                            category=category, preview=preview_url)
            
            story_db.publish()
            previous_chapter = None
            
            for c,i in zip(chapters, range(len(chapters))):    
                background_url = ''
                if c['background']:
                    background_url = save_file(c['background'], story['name'] + '_' + c['name'] + '_' + 'background_image')

                chapter_db = Chapter.objects.create(name=c['name'].strip(), story=story_db, previous_chapter=previous_chapter,\
                                                    condition=c['condition'].strip(), order=i, background_image=background_url)

                chapter_db.save()
                previous_chapter = chapter_db
                for m, j in zip(c['messages'], range(len(c['messages']))):
                    
                    attached_url = ''
                    if m['attached']:
                        attached_url = save_file(m['attached'], story['name'] + '_chapter_' + str(i) + '_message_' + str(j))

                    message = Message.objects.create(chapter=chapter_db, character=m['character'].strip(),\
                                                    color=characters[m['character'].strip()], order=j, \
                                                    attached=attached_url,  text=m['text'].strip(),\
                                                    is_ad=m['is_ad'], is_break=m['is_break'], attached_type=m['attached_type'])
                    message.save()

            story_db.first_chapter = Chapter.objects.filter(story=story_db).order_by('order')[0]
            story_db.publish()
            story_dict = get_story_dict(story_db)
            return JsonResponse(story_dict)
    else:
        return HttpResponse('Admin`s premmissions is required.', status=403)


@csrf_exempt
def admin_edit_story(request, story_id):
    if request.user.is_superuser:
        if request.method == 'GET':
            categories = Category.objects.all().values('name')
            categories = list(map(dict, categories))
            story_db = get_object_or_404(Story, id=story_id)
            story_json = get_story_dict(story_db)
            story_json.pop("time_publication", None)
            story_json['characters'] = {}
            if 'chapters' in story_json.keys():
                for index in range(len(story_json['chapters'])):
                    story_json['chapters'][index]['messages'] = [model_to_dict(m) for m in Message.objects.filter(chapter__id=story_json['chapters'][index]['id']).order_by('order')]
                    for message in story_json['chapters'][index]['messages']:
                        print(message)
                        story_json['characters'][message['character']] = message['color']
            print(story_json)
            return render(request, 'edit_story.html', context={'story_json':str(story_json), 'categories':categories})
        elif request.method == 'POST':
            request_body = request.body if type(request.body) == type('') else request.body.decode('utf-8')
            
            story_db = get_object_or_404(Story, id = story_id)
            
            story = json.loads(request_body)
            chapters = story.pop('chapters')
            characters = story.pop('characters')
            preview = story.pop('preview')

            user = Profile.objects.filter(user__username=request.user.username)
            if not user:
                return JsonResponse({'error':'User not found.'}, status=404)
            user = user[0]
            
            if user != story_db.user:
                return JsonResponse({'error':'Premission denied'}, status=403)

            preview_url = ''
            if preview and not 'static' in preview:    
                preview_url = save_file(preview, story['name'] + '_preview_image')

            category = Category.objects.filter(name=story['category'].strip())
            if not category:
                category = Category.objects.create(name=story['category'].strip())
                category.save()
            else:
                category = category[0]
            
            story_db.name = story['name']
            story_db.category = get_object_or_404(Category,name=story['category'])
            if preview_url:
                story_db.preview = preview_url
            
            story_db.publish()
            previous_chapter = None
            
            for c,i in zip(chapters, range(len(chapters))):    
                
                background_url = ''
                if c['background'] and not 'static' in c['background']:
                    background_url = save_file(c['background'], story['name'] + '_' + c['name'] + '_' + 'background_image')
                
                chapter_db = ""
                
                if 'id' in c.keys():
                    chapter_db = get_object_or_404(Chapter,id=c['id'])
                    chapter_db.name = c['name'].strip()
                    chapter_db.condition = c['condition'].strip()
                    if background_url:
                        chapter_db.background_image = background_url
                else:
                    chapter_db = Chapter.objects.create(name=c['name'].strip(), story=story_db, previous_chapter=previous_chapter,\
                                                    condition=c['condition'].strip(), order=i, background_image=background_url)

                chapter_db.save()
                previous_chapter = chapter_db
                for m, j in zip(c['messages'], range(len(c['messages']))):
                    
                    attached_url = ''
                    if m['attached'] and not 'static' in m['attached']:
                        attached_url = save_file(m['attached'], story['name'] + '_chapter_' + str(i) + '_message_' + str(j))
                    message = ""
                    if 'id' in m and m['id'] != '':
                        message = get_object_or_404(Message, id=m['id'])
                        message.character =  m['character'].strip()
                        message.color = characters[m['character']].strip()
                        message.order = j
                        message.text = m['text'].strip()
                        message.is_ad = m['is_ad']
                        message.is_break = m['is_break']
                        if attached_url:
                            message.attached = attached_url
                            message.attached_type = m['attached_type']
                    else:
                        message = Message.objects.create(chapter=chapter_db, character=m['character'].strip(),\
                                                        color=characters[m['character'].strip()], order=j, \
                                                        attached=attached_url,  text=m['text'].strip(),\
                                                        is_ad=m['is_ad'], is_break=m['is_break'], attached_type=m['attached_type'])
                    message.save()

            story_db.first_chapter = Chapter.objects.filter(story=story_db).order_by('order')[0]
            story_db.publish()
            story_dict = get_story_dict(story_db)
            return JsonResponse(story_dict)
    else:
        return HttpResponse('Admin`s premmissions is required.', status=403)
